import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key, required this.title});

  final String title;
  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: HexColor("#FAFDFF"),
              child: SingleChildScrollView(
          child: SafeArea(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
               margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    height: 300,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/welcomepage/LogoKlinik 1.png'),
                       // fit: BoxFit.cover
                      )
                    ),
                  ),
              Container(
                margin: const EdgeInsets.only(left: 300, bottom: 200, top: 0),
                padding: EdgeInsets.only(top: 100),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/welcomepage/Ellipse 68.png')
                  )
                ),
              ),

              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(

                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/images/welcomepage/Vector 3.png'),
                              scale: 1234,
                              fit: BoxFit.fill
                      )


                ),

              )
              ]
                )
              )
                ],
              ),
            )
          ),
      ),
      ),
    );
  }
}