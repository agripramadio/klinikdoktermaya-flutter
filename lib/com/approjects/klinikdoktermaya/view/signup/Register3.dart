import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class Register3 extends StatefulWidget {
  const Register3 ({super.key, required this.title});

  final String title;
  @override
  State<Register3> createState() => _Register3State();
}

class _Register3State extends State<Register3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: HexColor("#FAFDFF"),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,

        title: StepProgressIndicator(
          totalSteps: 6,
          selectedColor: Colors.black,
          unselectedColor: Colors.white,
          currentStep: 3,
        ),
        centerTitle: true,
        flexibleSpace: Container(
          padding: EdgeInsets.only(top: 1000),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  // end: Alignment.topLeft,
                  colors: <Color>[
                    HexColor("#56BAED"),
                    HexColor("#FFFFFF")
                  ]
              )
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 30,
            ),
            const Text(
              "🔓 Set Your Password", style: TextStyle(
              // fontWeight: FontWeight.bold,
              fontSize: 30,
            ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
                "Create A Unique Password!"
            ),
            const SizedBox(
              height: 75,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Form(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 00),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(" ",
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                ),)
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          maxLines: 1,
                          decoration: InputDecoration(
                              hintText: "Buat Password Anda",
                              prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.black,
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                      10)
                              )
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 300),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(" ",
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          maxLines: 1,
                          decoration: InputDecoration(
                              hintText: "Konfirmasi Password",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                      10)
                              )
                          ),
                        ),
                        SizedBox(
                          height: 260,
                        ),
                        GestureDetector(
                          child: Container(
                            alignment: Alignment.center,
                            width: 350,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(50)),
                                gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: <Color>[
                                      HexColor("#56BAED"),
                                      HexColor("007DBD")
                                    ]
                                )
                            ),
                            child: const Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Text('Next',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),

                      ],
                    )
                )
              ],
            ),

          ],
        ),
      ),
    );
  }
}