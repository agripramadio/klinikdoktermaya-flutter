import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class Register4 extends StatefulWidget {
  const Register4 ({super.key, required this.title});

  final String title;
  @override
  State<Register4> createState() => _Register4State();
}

class _Register4State extends State<Register4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: HexColor("#FAFDFF"),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,

        title: StepProgressIndicator(
          totalSteps: 6,
          selectedColor: Colors.black,
          unselectedColor: Colors.white,
          currentStep: 4,
        ),
        centerTitle: true,
        flexibleSpace: Container(
          padding: EdgeInsets.only(top: 1000),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  // end: Alignment.topLeft,
                  colors: <Color>[
                    HexColor("#56BAED"),
                    HexColor("#FFFFFF")
                  ]
              )
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 30,
            ),
            const Text(
              "Isi Biodata Anda", style: TextStyle(
              // fontWeight: FontWeight.bold,
              fontSize: 30,
            ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
                "Data Kamu Dijamin Aman"
            ),
            const SizedBox(
              height: 75,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Form(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 200),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(" ",
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                ),)
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          maxLines: 1,
                          decoration: InputDecoration(
                              hintText: "Usia",
                              prefixIcon: Icon(
                                Icons.email,
                                color: HexColor("#56BAED"),
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                      10)
                              )
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 340,
                        ),
                        GestureDetector(
                          child: Container(
                            alignment: Alignment.center,
                            width: 350,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(50)),
                                gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: <Color>[
                                      HexColor("#56BAED"),
                                      HexColor("007DBD")
                                    ]
                                )
                            ),
                            child: const Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Text('Next',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),

                      ],
                    )
                )
              ],
            ),

          ],
        ),
      ),
    );
  }
}